#Josh Churchill
#Turtle Game
file = pickAFile()
filePic = makePicture(file)
picH = getHeight(filePic)
picW = getWidth(filePic)
habitat = makeWorld(picW,picH)
turtle = makeTurtle(habitat)
penUp(turtle)
moveTo(turtle,0,0)
drop(turtle,filePic)
turnToFace(turtle, picW, 0)
penDown(turtle)
movement = true
redList = list()
greenList = list()
cyanList = list()
blueList = list()
picAmount = getPixels(filePic)
for p in picAmount:
  addList = list()
  pColor = getColor(p)
  kill = makeColor(255,0,0)
  telEnter = makeColor(0,255,0)
  telExit = makeColor(0, 255, 255)
  endGame = makeColor(0, 0, 255)
  if distance(pColor, kill) == 0:
    x = getX(p)
    y = getY(p)
    addList.append(x)
    addList.append(y)
    redList.append(addList)
  if distance(pColor, telEnter) == 0:
    x = getX(p)
    y = getY(p)
    addList.append(x)
    addList.append(y)
    greenList.append(addList)
  if distance(pColor, telExit) == 0:
    x = getX(p)
    y = getY(p)
    addList.append(x)
    addList.append(y)
    cyanList.append(addList)
  if distance(pColor, endGame) == 0:
    x = getX(p)
    y = getY(p)
    addList.append(x)
    addList.append(y)
    blueList.append(addList)
  del addList
while movement:
  coordinate = list()
  wasd = raw_input("Move that turtle ")
  if(wasd == "w"):
    forward(turtle, 25)
  if(wasd == "s"):
    backward(turtle, 25)
  if(wasd == "a"):
    turnLeft(turtle)
    forward(turtle, 25)
  if(wasd == "d"):
    turnRight(turtle)
    forward(turtle, 25)
  x = getXPos(turtle)
  y = getYPos(turtle)
  coordinate.append(x)
  coordinate.append(y)
  if(coordinate in redList):
    print("Your turtle died")
    movement = false
  if(coordinate in greenList):
    penUp(turtle)
    moveTo(turtle,cyanList[0][0],cyanList[0][1])
    penDown(turtle)
  if(coordinate in blueList):
    print("Congratulations! You made it to the exit!")
    movement = false 

  
    
  
  